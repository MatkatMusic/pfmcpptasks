#include <functional>
#include <memory>
#include <iostream>

//this example omits all of the overloaded operators.  
//you should still include them from your previous task.
//====================================================
template<typename NumericType>              // #2)
struct Numeric
{
    using Type = NumericType;               // #3) 
private:
    std::unique_ptr<Type> un;        // #1) 
public:
    Numeric(Type v) : un( std::make_unique<Type>(v) ) { }
    
    Numeric& apply( std::function<Numeric&(std::unique_ptr<Type>&)> f)   
    {
        std::cout << "std::function<>" << std::endl;
        if( f )
        {
            return f(un);  
        }
        
        return *this;
    }
    
    Numeric& apply( void(*f)(std::unique_ptr<Type>&) ) 
    {
        std::cout << "free function" << std::endl;
        if( f )
            f(un); 
            
        return *this; 
    }
    
    operator Type() { return *un; }
};
//====================================================
template<>                                          // #6)
struct Numeric<double>
{
    using Type = double;
private:
    std::unique_ptr<Type> ud;
public:
    Numeric(double d) : ud( new Type(d) ) { }
    
    template<typename Callable>                         // #7)
    Numeric& apply(Callable&& f)
    {
        f(ud);  
        
        return *this;
    }
    
    operator Type() { return *ud; }
};
//====================================================
template<typename NumericType>                                  // #5)
void cube( std::unique_ptr<NumericType>& un )
{
    NumericType& i = *un;
    i = i * i * i; //#8 
}
//====================================================
int main()
{
    Numeric<int> i(3);
    
    std::cout << "NumericWrapper: " << i << std::endl;
    
    using NumericType = decltype(i)::Type;                          // #4)
    using ReturnType = decltype(i);
    
    i.apply( [&i](std::unique_ptr<NumericType>& ui) -> ReturnType&  // #4)
    {
        *ui = *ui * *ui; 
        return i;
    });
    
    std::cout << "square Int (lambda): " << i << std::endl; 
    
    i.apply( cube );
    
    std::cout << "cube Int: " << i << std::endl; 
    
    std::cout << "//================================" << std::endl;
    std::cout << "//================================" << std::endl;
    std::cout << "//================================" << std::endl;
    
    Numeric<double> d(4.5);                              // #8)
    
    d.apply( [&d](std::unique_ptr<double>& ud )                 // #9)
    {
        double& ref = *ud;
        ref = ref * ref;
        std::cout << "explicit lambda result " << d << std::endl;   // #10) 
    });
    
    d.apply( cube<double> );                                    //#9) 
    
    std::cout << "explicit cube result " << d << std::endl;
    
    return 0;   
}