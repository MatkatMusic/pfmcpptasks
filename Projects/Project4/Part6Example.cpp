#include <iostream>

//example:

#include <functional>
#include <memory>
namespace Example {
    //====================================================
    /*
     note: this example omits the math operators.
     your task should still include them.
     */
    struct Int
    {
        // Int(int v) : ui( new int(v) ) { }        //old style of member init with explicit 'new'
        Int(int v) : ui( std::make_unique<int>(v) ) {} //new style of member init with make_unique instead
        
        Int& apply( std::function<Int&(std::unique_ptr<int>&)> f)   // 1), 2), 4)
        {
            std::cout << "std::function<>" << std::endl;
            
            if( f )
            {
                return f(ui);  // 5)
            }
            
            return *this; // 1)
        }
        
        Int& apply( void(*f)(std::unique_ptr<int>&) ) // 1), 3), 4)
        {
            std::cout << "free function" << std::endl;
            
            if( f )
                f(ui); // 5)
            
            return *this; // 1)
        }
        
        operator int() { return *ui; }
    private:
        std::unique_ptr<int> ui;
    };
    //====================================================
    void cube( std::unique_ptr<int>& ui )
    {
        int& i = *ui;
        i = i * i * i; // 7)
    }
    //====================================================
    int main()
    {
        Int i(3);
        
        std::cout << "Int: " << i << std::endl;
        
        i.apply( [&](std::unique_ptr<int>& ui) -> Int& // 6a)
                {
                    *ui = *ui * *ui; // 7)
                    return i;
                });
        
        std::cout << "square Int (lambda): " << i << std::endl;  // 8)
        
        i.apply( cube ); // 6b)
        
        std::cout << "cube Int: " << i << std::endl; // 9)
        
        return 0;
    }
} //end namespace Example

int main()
{
    Example::main();
}
