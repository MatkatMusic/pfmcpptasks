/*
 CH3_P04: 'new' example.
 */

#include "LeakedObjectDetector.h"

struct A { };
struct B { };
struct C { };

struct MyUDT_1 //My class def from the 'this' video that depends on some other UDTs
{
    struct Nested
    {
        B& b1, &b2;  //Nested depends on two B instances
        Nested(B& _1, B& _2) : b1(_1), b2(_2) { }
    };
    
    MyUDT_1(A& _a_, C* _c_) : a(_a_), c(_c_) { }
    A& a; //MyUDT_1 depends on an instance of A
    C* c = nullptr;
    
    B b1, b2;
    Nested nested{ b1, b2 }; //'nested' depends on b1 and b2
    
    JUCE_LEAK_DETECTOR(MyUDT_1) // <-- added the leak detector
};

struct UDTWrapper
{
    UDTWrapper(MyUDT_1* _udt) : udt1(_udt) { }
    ~UDTWrapper() { delete udt1; }
    MyUDT_1* udt1 = nullptr;
};

int main()
{
    A a;
    C c;
    /*
     here's the old usage from the 'this' video
     */
    MyUDT_1 udt1(a, &c);
    
    if( &udt1.b1 == &udt1.nested.b1 )
        std::cout << "they're the same object" << std::endl;
    
    /*
     here's the wrapper class usage, from the 'new' video
     */
    UDTWrapper udt1Wrapper( new MyUDT_1(a, &c) );
    
    if( &udt1Wrapper.udt1->b1 == &udt1Wrapper.udt1->nested.b1 )
        std::cout << "they're the same object" << std::endl;
    
    /*
     this demonstrates the leak detector that was added
     You do not need to do this in your task.
     if you didn't write safe/correct heap object usage, the leak detector will tell you.
     */
    auto* leakedUDT = new MyUDT_1(a, &c);
    leakedUDT->c = &c;
    
    return 0;
}
